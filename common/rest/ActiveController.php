<?php

declare(strict_types=1);

namespace common\rest;

abstract class ActiveController extends \yii2kernel\rest\ActiveController
{
}
