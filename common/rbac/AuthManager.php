<?php

declare(strict_types=1);

namespace common\rbac;

class AuthManager extends \yii2kernel\rbac\AuthManager
{
    public const ROLE_GUEST  = 'guest';
    public const ROLE_ADMIN  = 'admin';
    public const ROLE_CLIENT = 'client';

    public const ENTER_BACKEND  = 'enter-backend';
    public const ENTER_FRONTEND = 'enter-frontend';

    public static function roleLabels(): array
    {
        return [
            self::ROLE_GUEST  => \Yii::t('app', 'Guest'),
            self::ROLE_ADMIN  => \Yii::t('app', 'Administrator'),
            self::ROLE_CLIENT => \Yii::t('app', 'Client'),
        ];
    }
}
