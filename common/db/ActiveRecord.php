<?php

declare(strict_types=1);

namespace common\db;

class ActiveRecord extends \yii2kernel\db\ActiveRecord
{
    public static function find(): \common\db\ActiveQuery
    {
        return new ActiveQuery(static::class);
    }
}
