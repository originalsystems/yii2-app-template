<?php

declare(strict_types=1);

$rawRules = [
    '<module:(api)>'                                     => '<module>/default/index',
    '<module:(api)>/<controller:[\w-]+>'                 => '<module>/<controller>/index',
    '<module:(api)>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',

    '<module:(cabinet)>'                                     => '<module>/default/index',
    '<module:(cabinet)>/<controller:[\w-]+>'                 => '<module>/<controller>/index',
    '<module:(cabinet)>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',

    ''                                    => 'site/index',
    '<controller:[\w-]+>'                 => '<controller>/index',
    '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',
];

$rules = [];

foreach ($rawRules as $rule => $route) {
    $rules["<_lang:(\w\w)>/{$rule}"] = $route;
    $rules[$rule]                    = $route;
}

return $rules;
