<?php

declare(strict_types=1);

return \yii\helpers\ArrayHelper::merge([
    'id'             => \getenv('APP_CODE'),
    'basePath'       => APP_ROOT,
    'components'     => [
        'authManager' => [
            'class' => \common\rbac\AuthManager::class,
            'cache' => 'cache',
        ],
        'cache'       => [
            'class'     => \yii\redis\Cache::class,
            'redis'     => 'redis',
            'keyPrefix' => 'cache:',
        ],
        'db'          => [
            'class'               => \yii\db\Connection::class,
            'dsn'                 => 'pgsql:host=db;dbname=' . \getenv('DB_NAME') . ';port=5432',
            'username'            => \getenv('DB_USER'),
            'password'            => \getenv('DB_PASS'),
            'charset'             => 'utf8',
            'enableSchemaCache'   => true,
            'schemaCacheDuration' => 3600,
            'schemaCache'         => 'cache',
        ],
        'i18n'        => [
            'class'        => \yii\i18n\I18N::class,
            'translations' => [
                'yii2kernel' => [
                    'class'          => \yii\i18n\PhpMessageSource::class,
                    'basePath'       => '@yii2kernel/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'app'        => [
                    'class'          => \yii\i18n\PhpMessageSource::class,
                    'basePath'       => '@app/messages',
                    'sourceLanguage' => 'en-US',
                ],
            ],
        ],
        'redis'       => [
            'class'    => \yii\redis\Connection::class,
            'hostname' => 'redis',
            'database' => 0,
            'port'     => 6379,
        ],
        'session'     => [
            'class'     => \yii\redis\Session::class,
            'keyPrefix' => 'session:',
            'redis'     => 'redis',
            'timeout'   => 2592000,
        ],
        'urlManager'  => [
            'class'               => \yii\web\UrlManager::class,
            'enablePrettyUrl'     => true,
            'enableStrictParsing' => false,
            'normalizer'          => [
                'class'                  => \yii\web\UrlNormalizer::class,
                'collapseSlashes'        => true,
                'normalizeTrailingSlash' => true,
            ],
            'rules'               => require __DIR__ . '/rules.php',
            'showScriptName'      => false,
            'suffix'              => '/',
        ],
    ],
    'language'       => 'ru',
    'languages'      => ['ru', 'en'],
    'name'           => 'Yii 2 app template',
    'params'         => [],
    'runtimePath'    => '@common/runtime',
    'sourceLanguage' => 'en',
], file_exists(__DIR__ . '/main.local.php') ? require __DIR__ . '/main.local.php' : []);
