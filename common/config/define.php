<?php

declare(strict_types=1);

\define('PHP_BIN', \getenv('PHP_BIN') ? : '/usr/bin/php');

\define('APP_ROOT', \dirname(\dirname(__DIR__)));
\define('WEB_ROOT', APP_ROOT . '/frontend/web');

\define('DIR_MODE', 02775);
\define('FILE_MODE', 0664);

\define('YII_DEBUG', (bool) \getenv('APP_DEBUG'));
\define('YII_ENV', \getenv('APP_ENV'));
