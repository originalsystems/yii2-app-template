<?php

declare(strict_types=1);

if (class_exists(Kint::class)) {
    Kint::$enabled_mode = !YII_ENV_PROD;

    function kint()
    {
        $args = func_get_args();

        return call_user_func_array([Kint::class, 'dump'], $args);
    }
}
