<?php

declare(strict_types=1);

use yii\BaseYii;
use yii\di\Container;

require APP_ROOT . '/vendor/yiisoft/yii2/BaseYii.php';

class Yii extends BaseYii
{
    /**
     * @var \common\console\Application|\common\web\Application
     */
    public static $app;
    public static $aliases = [
        '@yii'          => APP_ROOT . '/vendor/yiisoft/yii2',
        '@yii2kernel'   => APP_ROOT . '/vendor/originalsystems/yii2-app-kernel',
        '@node_modules' => APP_ROOT . '/node_modules',
        '@common'       => APP_ROOT . '/common',
        '@console'      => APP_ROOT . '/console',
        '@frontend'     => APP_ROOT . '/frontend',
        '@upload'       => WEB_ROOT . '/upload',
        '@webroot'      => WEB_ROOT,
        '@web'          => '',
    ];

    /**
     * @var \common\models\User
     */
    public static $user;
}

spl_autoload_register(['Yii', 'autoload'], true, true);

$coreMap    = require APP_ROOT . '/vendor/yiisoft/yii2/classes.php';
$extendMap  = require APP_ROOT . '/vendor/originalsystems/yii2-app-kernel/classes.php';
$projectMap = require __DIR__ . '/classes.php';

\Yii::$classMap  = array_merge($coreMap, $extendMap, $projectMap);
\Yii::$container = new Container();
