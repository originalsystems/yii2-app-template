window.App = (function() {
    'use strict';

    return {
        /**
         * @param {string} input
         * @returns {string}
         */
        encode(input) {
            return ('' + input)
                .replace(/&/g, '&amp;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;')
                .replace(/"/g, '&#34;')
                .replace(/'/g, '&#39;');
        },

        /**
         * @param {string} input
         * @returns {string}
         */
        decode(input) {
            return ('' + input)
                .replace(/&lt;/g, '<')
                .replace(/&gt;/g, '>')
                .replace(/&#34;/g, '"')
                .replace(/&#39;/g, '\'')
                .replace(/&amp;/g, '&');
        },

        /**
         * @param {string} url
         * @param {string|object} data
         * @param {string} dataType
         * @returns {Promise}
         */
        get(url, data = {}, dataType = 'json') {
            return this.ajax('get', url, data, dataType);
        },

        /**
         * @param {string} url
         * @param {string|object} data
         * @param {string} dataType
         * @returns {Promise}
         */
        post(url, data = {}, dataType = 'json') {
            return this.ajax('post', url, data, dataType);
        },

        /**
         * @param {string} method
         * @param {string} url
         * @param {string|object} data
         * @param {string} dataType
         * @returns {Promise}
         */
        ajax(method, url, data = {}, dataType = 'json') {
            return new Promise(function(resolve, reject) {
                let options = {
                    type:     method,
                    url:      url,
                    data:     data,
                    dataType: dataType,
                    success:  function(json) {
                        if (json.code !== 0) {
                            reject(json.error);

                            return;
                        }

                        resolve(json.data);
                    },
                    error:    function() {
                        reject('Unexpected error');
                    }
                };

                if (data instanceof FormData) {
                    options.cache       = false;
                    options.processData = false;
                    options.contentType = false;
                }

                jQuery.ajax(options);
            });
        },

        /**
         * @param {string|null} title
         */
        wait(title) {
            let $body = jQuery('body');

            if (jQuery.fn.waitMe) {
                $body.waitMe({
                    effect: 'bounce',
                    text:   title,
                    bg:     'rgba(0,0,0,0.75)',
                    color:  'white'
                });
            }
        },

        resume() {
            let $body = jQuery('body');

            if (jQuery.fn.waitMe) {
                $body.waitMe('hide');
            }
        }
    };
})();
