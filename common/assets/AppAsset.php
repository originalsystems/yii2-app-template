<?php

declare(strict_types=1);

namespace common\assets;

use yii\web\JqueryAsset;
use yii2kernel\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/app';

    public $js = [
        'app.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
