<?php

declare(strict_types=1);

namespace common\models;

use yii\db\ActiveQuery;
use yii\web\IdentityInterface;
use yii2kernel\behaviors\HashBehavior;
use yii2kernel\behaviors\TimestampBehavior;
use yii2kernel\mixin\JsonProperties;
use yii2kernel\mixin\StatusLabel;
use common\db\ActiveRecord;

/**
 * @property int    $id
 * @property int    $image_id
 * @property int    $status
 * @property string $code
 * @property string $login
 * @property string $email
 * @property array  $fields
 * @property int    $created_at
 * @property int    $updated_at
 *
 * @property File   $image
 *
 * @property string $security_password_hash
 * @property string $security_password_reset_token
 * @property string $security_auth_key
 * @property string $security_access_token
 */
class User extends ActiveRecord implements IdentityInterface
{
    use JsonProperties;
    use StatusLabel;

    public const STATUS_ACTIVE   = 'active';
    public const STATUS_INACTIVE = 'inactive';
    public const STATUS_DELETED  = 'deleted';

    /**
     * @var \common\models\User[]
     */
    private static $_user_list = [];

    public static function tableName(): string
    {
        return 'public.user';
    }

    public static function statusLabels(): array
    {
        return [
            self::STATUS_ACTIVE   => \Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => \Yii::t('app', 'Inactive'),
            self::STATUS_DELETED  => \Yii::t('app', 'Deleted'),
        ];
    }

    public static function findIdentity($id)
    {
        $identity = self::findUser((int) $id);

        if ($identity !== null && $identity->status !== self::STATUS_ACTIVE) {
            return null;
        }

        return $identity;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        $query = self::find();

        $query->andWhere(['"user"."status"' => self::STATUS_ACTIVE]);
        $query->andWhere('"user"."fields" @> :ACCESS_TOKEN_JSON', [
            ':ACCESS_TOKEN_JSON' => json_encode(['security_access_token' => $token]),
        ]);

        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $query->one();
    }

    public static function findUser(int $id = null): ?User
    {
        if (empty($id)) {
            return null;
        }

        if (array_key_exists($id, self::$_user_list) === false) {
            $isCurrent = isset(\Yii::$user->id) && $id === \Yii::$user->id;

            self::$_user_list[$id] = $isCurrent ? \Yii::$user : static::find()->andWhere(['id' => $id])->one();
        }

        return self::$_user_list[$id];
    }

    public static function jsonProperties(): array
    {
        return [
            'security_auth_key',
            'security_password_hash',
            'security_password_reset_token',
            'security_access_token',
        ];
    }

    public static function labels(): array
    {
        return [
            'id'                            => \Yii::t('app', 'ID'),
            'image'                         => \Yii::t('app', 'Image'),
            'image_id'                      => \Yii::t('app', 'Image'),
            'status'                        => \Yii::t('app', 'Status'),
            'name'                          => \Yii::t('app', 'Name'),
            'login'                         => \Yii::t('app', 'Login'),
            'email'                         => \Yii::t('app', 'E-mail'),
            'security_auth_key'             => \Yii::t('app', 'Authorization key'),
            'security_password_hash'        => \Yii::t('app', 'Password hash'),
            'security_password_reset_token' => \Yii::t('app', 'Password reset token'),
            'security_access_token'         => \Yii::t('app', 'Access token'),
            'created_at'                    => \Yii::t('app', 'Created at'),
            'updated_at'                    => \Yii::t('app', 'Updated at'),
        ];
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey(): string
    {
        return $this->security_auth_key;
    }

    public function validateAuthKey($authKey): bool
    {
        return $this->getAuthKey() === $authKey;
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
            [
                'class'     => HashBehavior::class,
                'length'    => 12,
                'attribute' => 'code',
            ],
        ];
    }

    public function rules(): array
    {
        $statusLabels = self::statusLabels();

        return [
            [['status', 'email'], 'required'],
            [['image_id'], 'integer'],
            [
                ['image_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => File::class,
                'targetAttribute' => ['image_id' => 'id'],
            ],
            [['status'], 'in', 'range' => \array_keys($statusLabels)],
            [['email'], 'email'],
            [['login', 'email'], 'unique'],
        ];
    }

    public function fields()
    {
        return [
            'id',
            'image' => function (User $user) {
                return $user->image ? $user->image->getWebUrl(true) : null;
            },
            'status',
            'code',
            'login',
            'email',
            'created_at',
            'updated_at',
        ];
    }

    public function getImage(): ActiveQuery
    {
        return $this->hasOne(File::class, ['id' => 'image_id'])->andWhere(['content_type' => File::$imageMimeTypes]);
    }
}
