<?php

declare(strict_types=1);

namespace common\models;

use common\db\ActiveQuery;

/**
 * @property int    $id
 * @property string $content_type
 * @property string $original_name
 * @property string $path
 * @property string $created_at
 *
 * @property string $thumbUrl
 * @property string $webUrl
 * @property string $realPath
 *
 * @see     File::__get
 * @property string $thumbUrl60
 * @property string $thumbUrl120
 * @property string $thumbUrl240
 * @property string $thumbUrl360
 */
class File extends \yii2kernel\models\File
{
    public static function find(): ActiveQuery
    {
        return new ActiveQuery(static::class);
    }
}
