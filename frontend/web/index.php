<?php

declare(strict_types=1);

if (stripos($_SERVER['HTTP_USER_AGENT'] ?? '', 'MSIE') !== false) {
    http_response_code(301);
    header('Location: /old-browser/');

    exit;
}

require dirname(dirname(__DIR__)) . '/vendor/autoload.php';

(new \Dotenv\Dotenv(dirname(dirname(__DIR__)), '.env'))->load();

require dirname(dirname(__DIR__)) . '/common/config/define.php';
require APP_ROOT . '/common/Yii.php';
require APP_ROOT . '/common/config/bootstrap.php';
require APP_ROOT . '/frontend/config/bootstrap.php';

$config = require APP_ROOT . '/frontend/config/main.php';

$application = new common\web\Application($config);
$application->run();
