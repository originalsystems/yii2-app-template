<?php

/**
 * @var \common\web\View          $this
 * @var \yii\bootstrap\ActiveForm $form
 * @var \frontend\forms\SignUp    $formModel
 */

declare(strict_types=1);

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title         = \Yii::t('app', 'Sign up');
$this->breadcrumbs[] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= \Yii::t('app', 'The following fields are required for registration of a new user.'); ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($formModel, 'login')->textInput([
                'autocomplete' => 'off',
            ]); ?>

            <?= $form->field($formModel, 'email')->textInput([
                'autocomplete' => 'off',
            ]); ?>

            <?= $form->field($formModel, 'password')->passwordInput([
                'autocomplete' => 'off',
            ]); ?>

            <?= $form->field($formModel, 'password_repeat')->passwordInput([
                'autocomplete' => 'off',
            ]); ?>

            <div class="form-group">
                <?= Html::submitButton(\Yii::t('app', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'signup-button']); ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
