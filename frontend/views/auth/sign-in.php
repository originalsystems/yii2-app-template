<?php

/**
 * @var \common\web\View          $this
 * @var \yii\bootstrap\ActiveForm $form
 * @var \frontend\forms\SignIn    $formModel
 */

declare(strict_types=1);

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title         = \Yii::t('app', 'Sign in');
$this->breadcrumbs[] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= \Yii::t('app', 'Fill in your login details for sign in.') ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($formModel, 'login')->textInput(); ?>

            <?= $form->field($formModel, 'password')->passwordInput(); ?>

            <?= $form->field($formModel, 'remember_me')->checkbox(); ?>

            <div class="form-group">
                <?= Html::submitButton(\Yii::t('app', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
