<?php

/**
 * @var \common\web\View $this
 */

declare(strict_types=1);

$menu = [
    [
        'label' => Yii::t('app', 'Sign in'),
        'url'   => ['/auth/sign-in'],
    ],
    [
        'label' => Yii::t('app', 'Sign up'),
        'url'   => ['/auth/sign-up'],
    ],
];

if (Yii::$app->getUser()->getIsGuest() === false) {
    $menu = [
        [
            'label' => Yii::t('app', 'Cabinet'),
            'url'   => ['/cabinet/default/index'],
        ],
        [
            'label' => Yii::t('app', 'Sign out'),
            'url'   => ['/auth/sign-out'],
        ],
    ];
}
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <?= \yii\helpers\Html::a('<img src="/img/logo.png">', ['/site/index'], [
            'class' => 'navbar-brand',
        ]); ?>

        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#header-navbar"
                aria-controls="header-navbar"
                aria-expanded="false"
                aria-label="<?= Yii::t('app', 'Toggle navigation'); ?>">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="header-navbar">
            <?= \yii\widgets\Menu::widget([
                'items'        => $menu,
                'itemOptions'  => [
                    'class' => 'nav-item',
                ],
                'linkTemplate' => '<a href="{url}" class="nav-link">{label}</a>',
                'options'      => [
                    'class' => 'navbar-nav ml-auto',
                ],
            ]); ?>
        </div>
    </div>
</nav>
