<?php

declare(strict_types=1);

Yii::setAlias('@api', APP_ROOT . '/frontend/modules/api');
Yii::setAlias('@cabinet', APP_ROOT . '/frontend/modules/cabinet');
