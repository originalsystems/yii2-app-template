<?php

declare(strict_types=1);

return \yii\helpers\ArrayHelper::merge(require APP_ROOT . '/common/config/main.php', [
    'bootstrap'           => ['log', 'user'],
    'controllerNamespace' => 'frontend\controllers',
    'components'          => [
        'assetManager' => [
            'class'           => \yii2kernel\web\AssetManager::class,
            'appendTimestamp' => true,
            'compressCss'     => YII_ENV_PROD,
            'compressJs'      => YII_ENV_PROD,
            'dirMode'         => DIR_MODE,
            'fileMode'        => FILE_MODE,
            'linkAssets'      => true,
        ],
        'errorHandler' => [
            'class'       => \yii2kernel\web\ErrorHandler::class,
            'errorAction' => 'site/error',
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => \yii\log\FileTarget::class,
                    'levels'  => ['error', 'warning'],
                    'logFile' => '@runtime/logs/frontend.log',
                    'logVars' => ['_GET', '_POST', '_FILES', '_SERVER'],
                ],
            ],
        ],
        'response'     => [
            'class' => \yii2kernel\web\Response::class,
        ],
        'request'      => [
            'class' => \yii2kernel\web\Request::class,
        ],
        'session'      => [
            'class'     => \yii\redis\Session::class,
            'keyPrefix' => 'session:',
        ],
        'view'         => [
            'class' => \common\web\View::class,
        ],
        'user'         => [
            'class'           => \yii2kernel\web\User::class,
            'identityClass'   => \common\models\User::class,
            'loginUrl'        => ['/auth/sign-in'],
            'enableAutoLogin' => true,
        ],
    ],
    'modules'             => [
        'api'     => [
            'class' => \api\Module::class,
        ],
        'cabinet' => [
            'class' => \cabinet\Module::class,
        ],
    ],
    'viewPath'            => '@frontend/views',
], file_exists(__DIR__ . '/main.local.php') ? require __DIR__ . '/main.local.php' : []);
