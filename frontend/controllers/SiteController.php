<?php

declare(strict_types=1);

namespace frontend\controllers;

use yii\web\ErrorAction;
use common\web\Controller;

class SiteController extends Controller
{
    public function actions(): array
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
