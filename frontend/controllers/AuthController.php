<?php

declare(strict_types=1);

namespace frontend\controllers;

use common\models\User;
use common\web\Controller;
use frontend\forms\SignIn;
use frontend\forms\SignUp;

class AuthController extends Controller
{
    /**
     * Logs in a user.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionSignIn()
    {
        $formModel = new SignIn();

        if ($formModel->load(\Yii::$app->getRequest()->post())) {
            if ($formModel->save()) {
                return $this->redirect(['/cabinet/default/index']);
            }
        }

        return $this->render('sign-in', [
            'formModel' => $formModel,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionSignOut()
    {
        \Yii::$app->user->logout();

        return $this->redirect(['/site/index']);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     * @throws \yii\db\Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidParamException
     */
    public function actionSignUp()
    {
        $model     = new User();
        $formModel = new SignUp($model);

        if ($formModel->load(\Yii::$app->getRequest()->post())) {
            if ($formModel->save() && \Yii::$app->getUser()->login($model)) {
                return $this->redirect(['/site/index']);
            }
        }

        return $this->render('sign-up', [
            'formModel' => $formModel,
        ]);
    }
}
