<?php

declare(strict_types=1);

namespace api;

use yii2kernel\rest\ErrorHandler;

class Module extends \yii2kernel\base\Module
{
    public $controllerNamespace = 'api\\controllers';

    public function init()
    {
        \Yii::$app->getErrorHandler()->unregister();

        \Yii::$app->set('errorHandler', [
            'class'       => ErrorHandler::class,
            'errorAction' => '/api/default/error',
        ]);

        \Yii::$app->getErrorHandler()->register();
    }
}
