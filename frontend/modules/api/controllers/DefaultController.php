<?php

declare(strict_types=1);

namespace api\controllers;

use yii\web\NotFoundHttpException;
use common\rest\Controller;

class DefaultController extends Controller
{
    public function actionError()
    {
        $exception = \Yii::$app->getErrorHandler()->exception ?? new NotFoundHttpException('Not found');

        return \Yii::$app->getErrorHandler()->convertExceptionToArray($exception);
    }
}