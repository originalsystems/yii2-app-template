<?php

declare(strict_types=1);

namespace api\controllers;

use yii2kernel\rest\Controller;

class UserController extends Controller
{
    public function accessRules(): array
    {
        return [
            [
                'allow'   => true,
                'actions' => ['me'],
                'roles'   => ['@'],
            ],
            [
                'allow' => false,
            ],
        ];
    }

    public function actionMe()
    {
        return \Yii::$user;
    }
}
