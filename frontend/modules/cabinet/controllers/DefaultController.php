<?php

declare(strict_types=1);

namespace cabinet\controllers;

use yii2kernel\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
