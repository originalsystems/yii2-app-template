<?php

declare(strict_types=1);

namespace cabinet;

use yii\filters\AccessControl;
use common\rbac\AuthManager;

class Module extends \yii2kernel\base\Module
{
    public $controllerNamespace = 'cabinet\controllers';
    public $layout              = 'main';

    public function behaviors(): array
    {
        return [
            [
                'class'  => AccessControl::class,
                'except' => ['default/error'],
                'rules'  => [
                    [
                        'allow' => true,
                        'roles' => [AuthManager::ENTER_FRONTEND],
                    ],
                ],
            ],
        ];
    }

    public function init(): void
    {
        parent::init();

        \Yii::$app->getUser()->loginUrl        = ['/auth/sign-in'];
        \Yii::$app->getErrorHandler()->backUrl = ['/cabinet/default/index'];
    }
}
