<?php

/**
 * @var \common\web\View $this
 */

declare(strict_types=1);

$menu = [
    [
        'label' => Yii::t('app', 'Cabinet dashboard'),
        'url'   => ['/cabinet/default/index'],
    ],
    [
        'label' => Yii::t('app', 'Sign out'),
        'url'   => ['/auth/sign-out'],
    ],
];
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <?= \yii\helpers\Html::a('<img src="/img/logo.png">', ['/cabinet/default/index'], [
            'class' => 'navbar-brand',
        ]); ?>

        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#header-navbar"
                aria-controls="header-navbar"
                aria-expanded="false"
                aria-label="<?= Yii::t('app', 'Toggle navigation'); ?>">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="header-navbar">
            <?= \yii\widgets\Menu::widget([
                'items'        => $menu,
                'itemOptions'  => [
                    'class' => 'nav-item',
                ],
                'linkTemplate' => '<a href="{url}" class="nav-link">{label}</a>',
                'options'      => [
                    'class' => 'navbar-nav ml-auto',
                ],
            ]); ?>
        </div>
    </div>
</nav>
