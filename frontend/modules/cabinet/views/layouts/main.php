<?php

/**
 * @var \common\web\View $this
 * @var string           $content
 */

declare(strict_types=1);

use yii\helpers\Html;
use cabinet\assets\LayoutAsset;

LayoutAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= \Yii::$app->language ?>">
<head>
    <meta charset="<?= \Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title ? : \Yii::$app->name); ?></title>
    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>

<header>
    <?= $this->render('partial/navigation'); ?>
</header>

<div class="page-wrapper">
    <div class="container">
        <?= $content; ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p>&copy; <?= Html::a(Yii::$app->name, ['/site/index']); ?> <?= date('Y'); ?></p>
    </div>
</footer>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
