<?php

declare(strict_types=1);

namespace frontend\assets;

use yii\bootstrap\BootstrapPluginAsset;
use yii\web\YiiAsset;
use yii2kernel\assets\FileDependAsset;
use yii2kernel\assets\FontsAsset;
use common\assets\AppAsset;

class LayoutAsset extends FileDependAsset
{
    public $sourcePath = '@frontend/assets/layout';
    public $depends    = [
        YiiAsset::class,
        BootstrapPluginAsset::class,
        AppAsset::class,
        FontsAsset::class,
    ];
}
