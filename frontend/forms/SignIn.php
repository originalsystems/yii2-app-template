<?php

declare(strict_types=1);

namespace frontend\forms;

use yii2kernel\base\FormModel;
use common\rbac\AuthManager;
use common\models\User;

/**
 * Sign in form
 */
class SignIn extends FormModel
{
    public $login;
    public $password;
    public $remember_me = true;

    public function rules(): array
    {
        return [
            [['login', 'password'], 'required'],
            [['remember_me'], 'boolean'],
            [['password'], 'validatePassword'],
        ];
    }

    public static function labels(): array
    {
        return [
            'login'       => \Yii::t('app', 'Login'),
            'password'    => \Yii::t('app', 'Password'),
            'remember_me' => \Yii::t('app', 'Remember me'),
        ];
    }

    public function validatePassword($attribute, $params): bool
    {
        if ($this->hasErrors()) {
            return false;
        }

        $user = $this->getUser();

        if ($user === null) {
            $this->addError($attribute, \Yii::t('app', 'Invalid login or password.'));

            return false;
        }

        $backend  = \Yii::$app->getAuthManager()->checkAccess($user->getId(), AuthManager::ENTER_BACKEND);
        $frontend = \Yii::$app->getAuthManager()->checkAccess($user->getId(), AuthManager::ENTER_FRONTEND);

        if ($backend === false && $frontend === false) {
            $this->addError($attribute, \Yii::t('app', 'Invalid login or password.'));

            return false;
        }

        try {
            $validPassword = \Yii::$app->getSecurity()->validatePassword($this->password, $user->security_password_hash);
        } catch (\Exception $ex) {
            $validPassword = false;
        }

        if ($validPassword === false) {
            $this->addError($attribute, \Yii::t('app', 'Invalid login or password.'));

            return false;
        }

        return true;
    }

    /**
     * @return boolean
     * @throws \yii\base\InvalidParamException
     */
    public function save(): bool
    {
        if ($this->validate() === false) {
            return false;
        }

        /**
         * Log in on month in case of checked "remember_me"
         */
        return \Yii::$app->getUser()->login($this->getUser(), $this->remember_me ? 2592000 : 0);
    }

    /**
     * @var User
     */
    private $_user = false;

    /**
     * @return User|null
     */
    protected function getUser(): ?\common\models\User
    {
        if ($this->_user === false) {
            $this->_user = User::find()->where(['login' => $this->login, 'status' => User::STATUS_ACTIVE])->one();
        }

        return $this->_user;
    }
}
