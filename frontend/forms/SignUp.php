<?php

declare(strict_types=1);

namespace frontend\forms;

use yii\base\Exception;
use yii2kernel\base\FormModel;
use common\rbac\AuthManager;
use common\models\User;

/**
 * Sign up form
 */
class SignUp extends FormModel
{
    public $login;
    public $email;
    public $password;
    public $password_repeat;

    /**
     * @var \common\models\User
     */
    protected $_model;

    public function __construct(User $model, array $config = [])
    {
        $this->_model = $model;

        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['login', 'email', 'password'], 'required'],
            [['login', 'email'], 'string', 'min' => 4, 'max' => 255],
            [
                ['login'],
                'unique',
                'targetClass' => User::class,
                'message'     => \Yii::t('app', 'This login has already been taken.'),
            ],

            [['email'], 'email'],
            [
                ['email'],
                'unique',
                'targetClass' => User::class,
                'message'     => \Yii::t('app', 'This email address has already been taken.'),
            ],

            [['password'], 'string', 'min' => 6],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false],
        ];
    }

    public static function labels(): array
    {
        return [
            'login'           => \Yii::t('app', 'Login'),
            'email'           => \Yii::t('app', 'E-mail'),
            'password'        => \Yii::t('app', 'Password'),
            'password_repeat' => \Yii::t('app', 'Repeat password'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\Exception
     */
    public function save(): bool
    {
        $security = \Yii::$app->getSecurity();

        if ($this->validate() === false) {
            return false;
        }

        $transaction = \Yii::$app->getDb()->beginTransaction();
        $model       = $this->_model;

        try {
            $model->status                 = User::STATUS_ACTIVE;
            $model->login                  = $this->login;
            $model->email                  = $this->email;
            $model->security_password_hash = $security->generatePasswordHash($this->password);
            $model->security_auth_key      = $security->generateRandomString(32);
            $model->security_access_token  = $security->generateRandomString(16);

            $model->strictSave();

            $client = \Yii::$app->getAuthManager()->getRole(AuthManager::ROLE_CLIENT);

            \Yii::$app->getAuthManager()->assign($client, $model->id);

            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollBack();
            $this->addErrors($model->getErrors());

            return false;
        }

        return true;
    }
}
