<?php

declare(strict_types=1);

use yii\log\FileTarget;

return \yii\helpers\ArrayHelper::merge(require APP_ROOT . '/common/config/main.php', [
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'console\controllers',
    'controllerMap'       => [
        'message' => [
            'class'            => \yii\console\controllers\MessageController::class,
            'sourcePath'       => '@app',
            'messagePath'      => '@app/messages',
            'languages'        => [
                'en',
                'ru',
            ],
            'translator'       => 'Yii::t',
            'sort'             => true,
            'overwrite'        => true,
            'removeUnused'     => true,
            'markUnused'       => false,
            'except'           => [
                '.git',
                '.gitignore',
                '.gitkeep',
                '/common/runtime',
                '/frontend/web',
                '/messages',
                '/node_modules',
                '/vendor',
            ],
            'only'             => [
                '*.php',
            ],
            'format'           => 'php',
            'catalog'          => 'messages',
            'ignoreCategories' => [
                'yii',
                'yii2kernel',
            ],
            'phpFileHeader'    => '',
            'phpDocBlock'      => false,
        ],
        'migrate' => [
            'class'         => \yii\console\controllers\MigrateController::class,
            'templateFile'  => '@yii2kernel/console/views/migration.php',
            'migrationPath' => '@console/migrations',
        ],
    ],
    'components'          => [
        'log'        => [
            'targets' => [
                [
                    'class'   => FileTarget::class,
                    'levels'  => ['error', 'warning'],
                    'logFile' => '@runtime/logs/console.log',
                ],
            ],
        ],
        'urlManager' => [
            'baseUrl'  => \getenv('APP_BASE_URL'),
            'hostInfo' => \getenv('APP_BASE_URL'),
        ],
    ],
]);
