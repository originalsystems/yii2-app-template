<?php

declare(strict_types=1);

class m170000_000000_auth_data extends \yii\db\Migration
{
    public function safeUp()
    {
        $auth = \Yii::$app->getAuthManager();

        $adminRole  = $auth->createRole('admin');
        $clientRole = $auth->createRole('client');

        $enterFrontendPerm = $auth->createPermission('enter-frontend');
        $enterBackendPerm  = $auth->createPermission('enter-backend');

        $auth->add($adminRole);
        $auth->add($clientRole);
        $auth->add($enterFrontendPerm);
        $auth->add($enterBackendPerm);

        $auth->addChild($adminRole, $enterBackendPerm);
        $auth->addChild($adminRole, $enterFrontendPerm);
        $auth->addChild($clientRole, $enterFrontendPerm);
    }

    public function safeDown()
    {
        $auth = \Yii::$app->getAuthManager();

        $auth->remove($auth->getRole('admin'));
        $auth->remove($auth->getRole('client'));

        $auth->remove($auth->getPermission('enter-frontend'));
        $auth->remove($auth->getPermission('enter-backend'));
    }
}
