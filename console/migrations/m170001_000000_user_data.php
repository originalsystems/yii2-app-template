<?php

declare(strict_types=1);

use yii\db\Migration;

class m170001_000000_user_data extends Migration
{
    public function safeUp()
    {
        $security = \Yii::$app->getSecurity();
        $auth     = \Yii::$app->getAuthManager();

        $adminRole  = $auth->getRole('admin');
        $clientRole = $auth->getRole('client');

        $this->insert('"public"."user"', [
            'status' => 'active',
            'code'   => 'admin',
            'login'  => 'admin',
            'email'  => 'admin@application.com',
            'fields' => [
                'security_auth_key'      => $security->generateRandomString(32),
                'security_password_hash' => $security->generatePasswordHash('admin-password'),
                'security_access_token'  => $security->generateRandomString(16),
            ],
        ]);

        $this->insert('"public"."user"', [
            'status' => 'active',
            'code'   => 'client',
            'login'  => 'client',
            'email'  => 'client@application.com',
            'fields' => [
                'security_auth_key'      => $security->generateRandomString(32),
                'security_password_hash' => $security->generatePasswordHash('client-password'),
                'security_access_token'  => $security->generateRandomString(16),
            ],
        ]);

        /**
         * @var int[] $users
         */
        $users = $this->getDb()->createCommand('SELECT "login", "id" FROM "public"."user"')->queryAll(PDO::FETCH_KEY_PAIR);

        $auth->assign($adminRole, $users['admin']);
        $auth->assign($clientRole, $users['client']);
    }

    public function safeDown()
    {
        $this->delete('"public"."user"', [
            'login' => [
                'admin',
                'client',
            ],
        ]);
    }
}
