# Yii2 application template #

Current template based on php7.1, nginx (with http_image_filter_module), PostgreSQL and redis.

## Create project ##

Creating project, based on current template possible with one composer command

```bash
composer create-project originalsystems/yii2-app-template
```

First steps after create new project:

* run `composer install`, it should create local config files for current instance of application.
* create PostgreSQL database for project
* edit `.env` file with your project instance configurations, like host name, db credentials and etc.
* *optional* run `./vendor/bin/generate-nginx-vhost` to generate nginx vhost configuration file, based on `.env` file configurations.
* *optional* run `./vendor/bin/generate-cron` to generate cron template for execute application tasks, placed in `./cron` directories. 
