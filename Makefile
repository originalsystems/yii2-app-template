build:
	cp -n .env.dist .env
	mkdir -p .docker/db/data .docker/php/cache .docker/redis/data

start: build
	docker-compose up -d

stop:
	docker-compose down

restart: stop start
