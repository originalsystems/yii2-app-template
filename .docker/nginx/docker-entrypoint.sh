#! /bin/sh

set -e

umask 0002

echo "Starting nginx daemon"

exec "$@"
