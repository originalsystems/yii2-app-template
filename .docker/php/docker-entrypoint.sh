#! /bin/sh

set -e

umask 0002

sudo composer self-update

composer global require fxp/composer-asset-plugin

(cd /var/www/html; composer install)

sudo service rsyslog start
sudo service postfix start

echo "Starting php-fpm daemon"

exec "$@"
