<?php

return [
    'Access token' => 'Access token',
    'Active' => 'Active',
    'Administrator' => 'Administrator',
    'Authorization key' => 'Authorization key',
    'Cabinet' => 'Cabinet',
    'Cabinet dashboard' => 'Cabinet dashboard',
    'Client' => 'Client',
    'Created at' => 'Created at',
    'Deleted' => 'Deleted',
    'E-mail' => 'E-mail',
    'Fill in your login details for sign in.' => 'Fill in your login details for sign in.',
    'Guest' => 'Guest',
    'ID' => 'ID',
    'Image' => 'Image',
    'Inactive' => 'Inactive',
    'Invalid login or password.' => 'Invalid login or password.',
    'Login' => 'Login',
    'Name' => 'Name',
    'Password' => 'Password',
    'Password hash' => 'Password hash',
    'Password reset token' => 'Password reset token',
    'Remember me' => 'Remember me',
    'Repeat password' => 'Repeat password',
    'Sign in' => 'Sign in',
    'Sign out' => 'Sign out',
    'Sign up' => 'Sign up',
    'Status' => 'Status',
    'Submit' => 'Submit',
    'The following fields are required for registration of a new user.' => 'The following fields are required for registration of a new user.',
    'This email address has already been taken.' => 'This email address has already been taken.',
    'This login has already been taken.' => 'This login has already been taken.',
    'Toggle navigation' => 'Toggle navigation',
    'Updated at' => 'Updated at',
];
